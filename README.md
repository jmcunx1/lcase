## lcase - Change character case of each line in a text file

lcase(1) was created for systems that lack tr(1).  All it
does is change the Upper/Lower Case of characters in a
Text File.

If tr(1) is a better choice, but its command line can be
a but hard to remember.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/lcase) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/lcase.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/lcase.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
